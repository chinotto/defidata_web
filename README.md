# Defi Data Website - Build with Next + Netlify Starter

[![Netlify Status](https://api.netlify.com/api/v1/badges/ed50f56e-4fc2-4c98-8b66-1e5074c6f3d3/deploy-status)](https://app.netlify.com/sites/next-starter/deploys)

This is a [Next.js](https://nextjs.org/) v10.0.6 project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) and set up to be instantly deployed to [Netlify](https://url.netlify.com/Bk4UicocL)!

This project is a very minimal starter that includes 2 sample components, a global stylesheet, a `netlify.toml` for deployment, and a `jsconfig.json` for setting up absolute imports and aliases.

## Defi Data

[DefiData](https://www.defidata.dev/) is a stand alone web application for viewing real time crypto twitter stats. The application pulls data through our proprietary API and then injects the result into the webpage. 


## Running

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3004](http://localhost:3004) with your browser to see the result.

